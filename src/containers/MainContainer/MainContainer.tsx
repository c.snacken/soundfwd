import React from 'react';

import "./MainContainer.scss";

interface IProps {
    className?: string;
}

export const MainContainer: React.FC<IProps> = ({ children }) => {

    return (
        <div className="container">
            {children}
        </div>
    )
};
