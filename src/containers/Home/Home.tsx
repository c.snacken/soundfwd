import React, { useEffect, useState } from 'react';

import { Search, SongsList, Spinner } from 'components';
import { MainContainer } from 'containers/MainContainer';

import "./Home.scss";

interface IProps {
    className?: string;
}

export const Home: React.FC<IProps> = () => {
    const [songs, setSongs] = useState(null);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        fetch('http://localhost:8000/songs')
            .then(res => {
                return res.json();
            }).then((data) => {
                setSongs(data)
                setLoading(false)
            }).catch((err) => {
                console.warn(err)
            })
    }, []);

    return (
        <MainContainer>
            <Search />
            {loading && <Spinner />}
            {songs && <SongsList songs={songs} />}
        </MainContainer>
    )
};
