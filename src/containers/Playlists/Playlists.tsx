import React from 'react';
import { NavLink, Outlet } from 'react-router-dom';

import { useDataLayerValue } from "context/DataLayer";
import { MainContainer } from 'containers/MainContainer';

import "./Playlists.scss";

interface IProps {
    className?: string;
}

export const Playlists: React.FC<IProps> = () => {
    const { state } = useDataLayerValue();

    return (
        <MainContainer>
            <nav className="playlists">
                {state?.playlists?.map(playlist => (
                    <NavLink
                        className={({ isActive }) => `playlists__item ${isActive && "playlists__item--active"}`}
                        to={`/playlists/${playlist.id}`}
                        key={playlist.id}
                    >
                        {playlist.name}
                    </NavLink>
                ))}
            </nav>
            <Outlet />
        </MainContainer>
    )
};
