import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Routes } from "react-router-dom";

import reportWebVitals from 'reportWebVitals';
import App from 'App';
import { AppProvider } from 'context/DataLayer';
import { Home } from 'containers/Home';
import { Playlists } from 'containers/Playlists';
import { Playlist } from 'components/Playlist';

import './index.css';

ReactDOM.render(
  <React.StrictMode>
    <AppProvider>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<App />}>
            <Route path="" element={<Home />} />
            <Route path="/playlists" element={<Playlists />}>
              <Route
                index
                element={
                  <main style={{ padding: "1rem" }}>
                    <p>Select a playlist</p>
                  </main>
                }
              />
              <Route path=":playlistId" element={<Playlist />} />
            </Route>
            <Route
              path="*"
              element={
                <main style={{ padding: "1rem" }}>
                  <p>Nothing here!</p>
                </main>
              }
            />
          </Route>
        </Routes>
      </BrowserRouter>
    </AppProvider>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
