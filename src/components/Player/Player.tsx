import React from 'react';

import "./Player.scss";

interface IProps {
    className?: string;
}

export const Player: React.FC<IProps> = ({ children }) => {

    return (
        <div className="player">
            <div className="player__body">
                {children}
            </div>

        </div>
    );
};
