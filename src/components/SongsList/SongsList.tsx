import React from 'react';

import { ISong } from 'interfaces/song';
import { SongRow } from './SongRow';

import "./SongsList.scss";

interface IProps {
    className?: string;
    songs: ISong[];
    inPlaylist?: boolean;
    playlistId?: string;
}

export const SongsList: React.FC<IProps> = ({ songs, inPlaylist, playlistId }) => {
    return (
        <div className="songs">
            {songs?.map((song) => (
                <SongRow key={song.id} song={song} inPlaylist={inPlaylist} playlistId={playlistId} />
            ))}
        </div>
    )
};
