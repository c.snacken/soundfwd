import React from 'react';
import { useNavigate } from 'react-router-dom';

import AddIcon from "@material-ui/icons/Add";
import RemoveIcon from "@material-ui/icons/Remove";
import { ISong } from "interfaces/song";
import { toggleSongFromPlaylist } from 'utils/playlists';
import { useDataLayerValue } from 'context/DataLayer';

import "./SongRow.scss";

interface IProps {
    className?: string;
    song: ISong;
    inPlaylist?: boolean;
    playlistId?: string;
}

//TODO add dropdown for playlist selector
export const SongRow: React.FC<IProps> = ({ song, inPlaylist, playlistId }) => {

    const { dispatch } = useDataLayerValue();
    const navigate = useNavigate();

    const onClick = () => {
        const id = playlistId || "Play12345";
        toggleSongFromPlaylist(song, id, inPlaylist, dispatch)
        if (!inPlaylist) {
            navigate("/playlists/" + id)
        }
    }

    return (
        <div className="song-row">

            <div className="song-row__info">
                <h1 className="song-row__title">{song.name}</h1>
                <p>
                    {song.artist} -{" "}
                    {song.name}
                </p>
            </div>
            <div role="button" className="song-row__button" onClick={onClick}>
                {inPlaylist ? <RemoveIcon /> : <AddIcon />}
            </div>
        </div>
    )
};
