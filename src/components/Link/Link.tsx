import React from 'react';
import { Link as RouterLink } from 'react-router-dom';

interface IProps {
    className?: string;
    to: string;
}

export const Link: React.FC<IProps> = ({
    children,
    className,
    to,
}) => {

    return (
        <RouterLink
            style={{ textDecoration: 'none' }}
            className={className}
            to={to}
        >
            {children}
        </RouterLink>
    )
};
