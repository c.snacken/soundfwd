import React from 'react';
import { NavLink } from 'react-router-dom';

import { useDataLayerValue } from "context/DataLayer";
import HomeIcon from "@material-ui/icons/Home";
import ListIcon from '@material-ui/icons/List';
import { Link } from 'components';
import { SidebarOption } from '../SidebarOption';

import "./Sidebar.scss";


interface IProps {
    className?: string;
}

export const Sidebar: React.FC<IProps> = () => {

    const { state } = useDataLayerValue();

    return (
        <div className="sidebar">
            <Link to="/"><SidebarOption title="Home" Icon={HomeIcon} /></Link>
            <br />
            <Link className="sidebar__item" to="/playlists"><SidebarOption title="Playlists" Icon={ListIcon} /></Link>
            {state?.playlists?.map((playlist) => (
                <NavLink className={({ isActive }) => `sidebar__sub-item ${isActive && "sidebar__sub-item--active"}`} key={playlist.id} to={`/playlists/${playlist.id}`}><SidebarOption title={playlist.name} /></NavLink>
            ))
            }
        </div >
    )
};

