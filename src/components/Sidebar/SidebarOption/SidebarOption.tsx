import React from 'react';
import "./SidebarOption.scss";

interface IProps {
    className?: string;
    Icon?: any;
    title: string;
}

export const SidebarOption: React.FC<IProps> = ({
    Icon,
    title,
}) => (
    <div className="sidebar-option">
        {Icon && <Icon className="sidebar-option__icon" />}
        {Icon ? <h4>{title}</h4> : <p className="sidebar-option__title">{title}</p>}
    </div>
);
