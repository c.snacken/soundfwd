import React from 'react';
import SearchIcon from '@material-ui/icons/Search';

import "./Search.scss";

interface IProps {
    className?: string;
}

export const Search: React.FC<IProps> = () => {
    return (
        <div className="search">
            <SearchIcon />
            <input placeholder="Search for Artists, Songs, or Albums" type="text" />
        </div>
    )
};
