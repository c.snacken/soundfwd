import React from 'react';

import "./Spinner.scss";

interface IProps {
    className?: string;
}

export const Spinner: React.FC<IProps> = () => {
    return (
        <div>
            <div className="spinner" />
        </div>
    )
};
