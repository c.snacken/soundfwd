import React from 'react';
import { useParams } from 'react-router-dom';

import { useDataLayerValue } from "context/DataLayer";
import { SongsList } from 'components/SongsList';
import { getPlaylist } from 'utils/playlists';

import "./Playlist.scss";

interface IProps {
    className?: string;
}

export const Playlist: React.FC<IProps> = () => {
    const { state } = useDataLayerValue();
    const params = useParams();

    const playlist = getPlaylist(params.playlistId, state.playlists);

    return (
        <div className="playlist">
            <h1>{playlist?.name}</h1>
            {playlist && <SongsList songs={playlist.songs} inPlaylist playlistId={playlist.id} />}
        </div>
    )
};
