export interface ISong {
    id: string,
    name: string;
    artist: string;
    shortname: string;
    bpm: number;
    duration: number;
    genre: string;
    spotifyId: string;
    album: string;
}
