import { IPlaylist } from 'interfaces/playlist';
import { ISong } from 'interfaces/song';

export interface IContext {
    user: null;
    token: null;
    playlists: IPlaylist[];
    playing: boolean;
    activeSong: ISong | null;
}
