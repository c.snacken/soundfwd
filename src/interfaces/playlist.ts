import { ISong } from "./song";

export interface IPlaylist {
    id: string,
    name: string;
    songs: ISong[];
}
