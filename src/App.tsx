import React, { useEffect } from 'react';
import { Outlet } from 'react-router-dom';

import { useDataLayerValue } from "context/DataLayer";
import { Sidebar, Player } from 'components';
import { getPlaylists } from 'utils/playlists';

import './App.scss';

function App() {

  const { dispatch } = useDataLayerValue();

  useEffect(() => {
    getPlaylists(dispatch);
  }, []);
  return (
    <div className="app">
      <Player>
        <Sidebar />
        <Outlet />
      </Player>
    </div>
  );
}

export default App;


