import { PlaylistsActions } from "context/reducer";
import { Types } from 'context/reducer';
import { IPlaylist } from "interfaces/playlist";
import { ISong } from "interfaces/song";

export const getPlaylists = (dispatch: React.Dispatch<PlaylistsActions>): void => {
    fetch('http://localhost:8000/playlists')
      .then(res => {
        return res.json();
      }).then((data) => {
        dispatch({
          type: Types.SetPlaylists,
          payload: { playlists: data },
        });
      })
}

export const getPlaylist = (playlistId: string | undefined, playlists: IPlaylist[]): IPlaylist | undefined => {
    return playlistId ? playlists.find(list => list.id === playlistId) : undefined;
}

export const toggleSongFromPlaylist = (song: ISong, playlistId: string, inPlaylist: boolean | undefined, dispatch: React.Dispatch<PlaylistsActions> ) => {
    dispatch({
        type: inPlaylist ? Types.RemoveSongFromPlaylist : Types.AddSongToPlaylist,
        payload: { song, playlistId },
      });
}
