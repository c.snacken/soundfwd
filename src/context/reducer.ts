import { v4 as uuid } from 'uuid';

import { IContext } from "interfaces/context";
import { IPlaylist } from "interfaces/playlist";
import { ISong } from "interfaces/song";

type ActionMap<M extends { [index: string]: any }> = {
[Key in keyof M]: M[Key] extends undefined
    ? {
        type: Key;
    }
    : {
        type: Key;
        payload: M[Key];
    }
};

export enum Types {
SetPlaylists = 'SET_PLAYLISTS',
AddPlaylist = 'ADD_PLAYLIST',
RemovePlaylist = 'REMOVE_PLAYLIST',
RemoveSongFromPlaylist = 'DELETE_SONG',
AddSongToPlaylist = 'ADD_SONG',
GetPlaylist = 'GET_PLAYLIST',
}

type PlaylistPayload = {
[Types.SetPlaylists] : {
    playlists: IPlaylist[];
};
[Types.AddPlaylist] : {
    name: string;
};
[Types.RemovePlaylist]: {
    id: string;
};
[Types.AddSongToPlaylist]: {
    song: ISong;
    playlistId: string;
};
[Types.RemoveSongFromPlaylist]: {
    song: ISong;
    playlistId: string;
};
}

export type PlaylistsActions = ActionMap<PlaylistPayload>[keyof ActionMap<PlaylistPayload>];

export const playlistsReducer = (state: IContext, action: PlaylistsActions) => {

switch (action.type) {
    case Types.SetPlaylists:
        return {
            ...state,
            playlists: [...action.payload.playlists],
        };
    case Types.AddPlaylist:
        return {
            ...state,
            playlists: [...state.playlists, {id: uuid(), name: action.payload.name, songs: []}],
        };

    case Types.RemovePlaylist:
        return {
            ...state,
            playlists: [...state.playlists.filter(( playlist ) => {
                    return playlist.id !== action.payload.id;
            })],
        };
    case Types.AddSongToPlaylist:
        return {
            ...state,
            playlists: [...addSong(action.payload.song, state.playlists, action.payload.playlistId)],
        };
    case Types.RemoveSongFromPlaylist:
    return {
        ...state,
        playlists: [...removeSong(action.payload.song, state.playlists, action.payload.playlistId)],
    };
    default:
        return state;
    }
}

export const addSong = (song: ISong, playlists: IPlaylist[], playlistId: string) => {
    const newPlaylists = [...playlists];
    const index = newPlaylists.findIndex((playlist => playlist.id === playlistId));
    const isInPlaylist = newPlaylists[index].songs.findIndex(s => s.id === song.id);

    if(isInPlaylist === -1) {
        newPlaylists[index].songs.push(song);
    }

    return newPlaylists;
}

export const removeSong = (song: ISong, playlists: IPlaylist[], playlistId: string) => {
    playlists.forEach((playlist) => {
        if (playlist.id === playlistId) {
            const index = playlist.songs.findIndex(s => s.id === song.id);

            if (index !== -1) {
                playlist.songs.splice(index, 1);
            }
        }
    })

    return playlists;
}

