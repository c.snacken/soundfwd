import React, { createContext, useReducer, Dispatch, useContext } from 'react';

import { PlaylistsActions, playlistsReducer } from 'context/reducer';
import { IContext } from 'interfaces/context';

const initialState: IContext = {
    user: null,
    token: null,
    playlists: [],
    playing: false,
    activeSong: null,
}

const AppContext = createContext<{
    state: IContext;
    dispatch: Dispatch<PlaylistsActions>;
}>({
    state: initialState,
    dispatch: () => null
});

const AppProvider: React.FC = ({ children }) => {
    //TODO multiple reducers
    const [state, dispatch] = useReducer(playlistsReducer, initialState);

    return (
        <AppContext.Provider value={{ state, dispatch }}>
            {children}
        </AppContext.Provider>
    )
}

export { AppProvider, AppContext };

// COMPONENTS USABLE HOOK
export const useDataLayerValue = () => useContext(AppContext);
